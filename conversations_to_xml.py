import json

# file containing conversations in json format
data_file = "twitter_conversations.txt"

# save file
save_file = "twitter_conversations.xml"

if __name__ == "__main__":

    hist_tpc = []
    hist_upc = []
    stats = {"number_of_conversations":          .0,
             "number_of_turns":                  .0,
             "number_of_turns_per_conversation": .0,
             "number_of_words":                  .0,
             "number_of_words_per_turn":         .0,
             "number_of_words_per_conversation": .0,
             "number_of_users":                  .0,
             "number_of_users_per_conversation": .0
             }
    total_users = {}
    with open(save_file, "w") as s:
        s.write("<dialog>\n")
        with open(data_file, "r") as f:
             for line in f:
                 # Conversation is a JSON string
                 data = json.loads(line)["conversation"]
                 # Ignore the singles
                 if len(data) > 1:
                     stats["number_of_conversations"] += 1
                     stats["number_of_turns"] += len(data)
                     hist_tpc += [len(data)]
                     # Convert into dicts
                     data  = [json.loads(i) for i in data]
                     s.write("<s>")
                     turn_id = 1
                     uids = {}
                     # Output the XML entries
                     for turn_data in reversed(data):
                         turn_text = turn_data["text"]
                         turn_uid  = turn_data["user"]["id"]
                         if not turn_uid in uids:
                             uids[turn_uid] = turn_id
                             turn_id += 1
                         if not turn_uid in total_users:
                             total_users[turn_uid] = 1
                             stats["number_of_users"] += 1
                         else:
                             total_users[turn_uid] += 1
                         stats["number_of_words"] += len(list(turn_text.split()))
                         xml = "<utt uid=\"%s\">%s</utt>" % (uids[turn_uid], turn_text)
                         s.write(xml.encode("utf-8"))
                     stats["number_of_users_per_conversation"] += turn_id
                     hist_upc += [turn_id]
                     s.write("</s>\n")
        s.write("</dialog>")

    stats["number_of_turns_per_conversation"] = stats["number_of_turns"] / stats["number_of_conversations"]
    stats["number_of_words_per_turn"] = stats["number_of_words"] / stats["number_of_turns"]
    stats["number_of_words_per_conversation"] = stats["number_of_words"] / stats["number_of_conversations"]
    stats["number_of_users_per_conversation"] = stats["number_of_users_per_conversation"] / stats["number_of_conversations"]

    print stats

    with open("user_per_conversation_histogram_values.txt", "w") as k:
        k.write('\n'.join(map(str, hist_upc)))
    with open("turn_per_conversation_histogram_values.txt", "w") as k:
        k.write('\n'.join(map(str, hist_tpc)))
