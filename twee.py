#Import the necessary methods from tweepy library
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy import API

#Variables that contains the user credentials to access Twitter API
access_token =  "ENTER YOUR ACCESS TOKEN"
access_token_secret = "ENTER YOUR ACCESS TOKEN SECRET"
consumer_key = "ENTER YOUR API KEY"
consumer_secret =  "ENTER YOUR API SECRET"

place_id_montreal = 3534
place_id_france = 23424819

geobox_quebec = [-79.76,44.99,-57.10,62.59]
geobox_france = [-5.225, 41.333, 9.55, 51.2]

#tweets_data_path = '../data/twitter_data.txt'
tweets_data_path = 'twitter_data_france.txt'

#This is a basic listener that just prints received tweets to stdout.
class StdOutListener(StreamListener):

    def __init__(self, api=None):
        super(StdOutListener, self).__init__()
        self.num_tweets = 0

    def on_data(self, data):
        self.num_tweets += 1
        print self.num_tweets
        print data
        #Write twitter data to file
        with open(tweets_data_path, 'a') as tf:
            tf.write(data)
        return True

    def on_error(self, status):
        print status


#Find and return a list of trending topics for a given location
def findTrends(api, place_id):
    trends = api.trends_place(place_id)[0]['trends']
    #print trends
    trend_names = [trend['name'] for trend in trends]
    #print trend_names
    return trend_names


if __name__ == '__main__':

    #This handles Twitter authentification and connection to Twitter Streaming API
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    l = StdOutListener()
    stream = Stream(auth, l)

    #Filter by trending topics
    #api = API(auth)
    #trends = findTrends(api, place_id_montreal)
    #stream.filter(languages=["fr"], track=trends)

    #Filter by location
    stream.filter(languages=["fr"],locations=geobox_france)
