from tweepy import OAuthHandler
from tweepy import API
import json

#Variables that contains the user credentials to access Twitter API
access_token =  "ENTER YOUR ACCESS TOKEN"
access_token_secret = "ENTER YOUR ACCESS TOKEN SECRET"
consumer_key = "ENTER YOUR API KEY"
consumer_secret =  "ENTER YOUR API SECRET"

# path to file containing tweets in json format
data_file = "twitter_data_france.txt"

# path to output file
save_file = "twitter_conversations.txt"

if __name__ == '__main__':

    # twitter authentication
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    # clear output file
    open(save_file, 'w').close()

    total_tweets = 0

    with open(data_file, "r") as f:
         for line in f:
              # parse each tweet
              tweet = json.loads(line)

               # check if tweet is part of a conversation (has a parent tweet)
              if tweet['in_reply_to_status_id']:
                   t = tweet
                   conversation = [json.dumps(t)]
                   conversation_len = 1
                   #print api.rate_limit_status()

                   # retrieve all parent tweets
                   while t['in_reply_to_status_id']:
                        try:
                            t = api.get_status(t['in_reply_to_status_id'])._json
                            conversation.append(json.dumps(t))
                            conversation_len += 1
                        except Exception:
                            break

                   # save conversations longer than one tweet to file
                   # note: list of tweets in a conversation is ordered newest -> oldest
                   if conversation_len > 1:
                        with open(save_file, "a") as sf:
                            sf.write(json.dumps({'conversation_len': conversation_len, 'conversation': conversation}))
                            sf.write("\n")

                        total_tweets += conversation_len

                   print total_tweets

